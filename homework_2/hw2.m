% =========================================================================
%  MATLAB Exercises for Homework 2
%
%  Erik Swenson
% =========================================================================
clear all; close all;

% Open the requested file and load it into three vectors x, y, and z
% representing the respective components of the acceleration over time.
the_file = fopen('../dataset/Climb_stairs/Accelerometer-2011-03-24-10-24-39-climb_stairs-f1.txt', 'r');
x = [];
y = [];
z = [];
data = [];

the_line = fgetl(the_file);
while ischar(the_line)
    % Convert the character array into integer values
    parts = str2num(the_line);
    x = [x, parts(1)];
    y = [y, parts(2)];
    z = [z, parts(3)];
    the_line = fgetl(the_file);
end

fclose(the_file);

% Convert the coded data [x, y, z] values to real values.
g = 9.8;
dec_x = (3.0 * g / 63.0) * x - (1.5 * g);
dec_y = (3.0 * g / 63.0) * y - (1.5 * g);
dec_z = (3.0 * g / 63.0) * z - (1.5 * g);
for i = 1:length(dec_x)
    data = [data, sqrt(dec_x(i) * dec_x(i) + dec_y(i) * dec_y(i) + dec_z(i) * dec_z(i))];
end

% Compute the energy of the signal.
s = sum(data .* data) % The .* operator computes element-wise multiplication

% Implement a moving average filter with a sliding window size 3.

% First, define the impulse response.
h = [1/3, 1/3, 1/3];

% Now, use the impulse response for each component to smooth out the data.
% TODO: Do this for the processed data as well as the raw data.
% TODO: The scale is being changed (probably because of keeping the same
%       dimensions).  Try 'valid' instead of 'same'. Or just force same
%       scale.
smooth_x = conv(dec_x, h, 'same');
smooth_y = conv(dec_y, h, 'same');
smooth_z = conv(dec_z, h, 'same');

f3 = figure;
set(f3, 'Name', 'Problem 3 Plots');
subplot(2, 3, 1);
plot(dec_x);
title('Original x')
subplot(2,3,2);
plot(dec_y);
title('Original y')
subplot(2,3,3);
plot(dec_z);
title('Original z')
subplot(2,3,4);
plot(smooth_x);
title('Smoothed x')
subplot(2,3,5);
plot(smooth_y);
title('Smoothed y')
subplot(2,3,6);
plot(smooth_z);
title('Smoothed z')
print('matlab_problem_3_plots','-dpdf')

% Begin Problem 4
y_x = zeros(1, length(dec_x));
y_y = zeros(1, length(dec_y));
y_z = zeros(1, length(dec_z));

y_x(1) = dec_x(1);
y_y(1) = dec_y(1);
y_z(1) = dec_z(1);

% Create the recursive system defined by y(n) = 0.8 * y(n-1) + x(n)
for i = 2:(length(y_x) - 1)
    y_x(i) = 0.8 * y_x(i-1) + dec_x(i);
    y_y(i) = 0.8 * y_y(i-1) + dec_y(i);
    y_z(i) = 0.8 * y_z(i-1) + dec_z(i);
end

% Plot the data for this recursive system.
f4 = figure;
set(f4, 'Name', 'Problem 4 Plots');
subplot(3,1,1);
plot(y_x);
subplot(3,1,2);
plot(y_y);
subplot(3,1,3);
plot(y_z);
print('matlab_problem_4_plots','-dpdf')

% Begin Problem 5
y_x = zeros(1, length(dec_x));
y_y = zeros(1, length(dec_y));
y_z = zeros(1, length(dec_z));

y_x(1) = dec_x(1);
y_y(1) = dec_y(1);
y_z(1) = dec_z(1);

% Create the recursive system defined by y(n) = (n/n+1)y(n-1) + (1/n+1)x(n)
for i = 2:(length(y_x) - 1)
    y_x(i) = ((i-1)/i) * y_x(i-1) + (1/i) * dec_x(i);
    y_y(i) = ((i-1)/i) * y_y(i-1) + (1/i) * dec_y(i);
    y_z(i) = ((i-1)/i) * y_z(i-1) + (1/i) * dec_z(i);
end

% Plot the data for this recursive system.
f5 = figure;
set(f5, 'Name', 'Problem 5 Plots');
subplot(3,1,1);
plot(y_x);
subplot(3,1,2);
plot(y_y);
subplot(3,1,3);
plot(y_z);
print('matlab_problem_5_plots','-dpdf')

% Begin Problem 6
y_x = zeros(1, length(dec_x));
y_y = zeros(1, length(dec_y));
y_z = zeros(1, length(dec_z));

y_x(1) = dec_x(1);
y_y(1) = dec_y(1);
y_z(1) = dec_z(1);
y_x(2) = y_x(1) + dec_x(2);
y_y(2) = y_y(1) + dec_y(2);
y_z(2) = y_z(1) + dec_z(2);

% Create the recursive system defined by y(n) = y(n-1) - y(n-2) + x(n)
for i = 3:(length(y_x) - 1)
    y_x(i) = y_x(i-1) - y_x(i-2) + dec_x(i);
    y_y(i) = y_y(i-1) - y_y(i-2) + dec_y(i);
    y_z(i) = y_z(i-1) - y_z(i-2) + dec_z(i);
end

% Plot the data for this recursive system.
f6 = figure;
set(f6, 'Name', 'Problem 6 Original Plots');
subplot(3,1,1);
plot(y_x);
subplot(3,1,2);
plot(y_y);
subplot(3,1,3);
plot(y_z);
print('matlab_problem_6_orig_plots','-dpdf')

% Test this system to see if it is linear. We will show that:
%     y( a*x1 + b*x2 ) = a * y(x1) + b * y(x2)

% Build up the left hand side.
test_input = 5 .* dec_x + 7 .* dec_y;
lhs_y = zeros(1, length(dec_x));
lhs_y(1) = test_input(1);
lhs_y(2) = lhs_y(1) + test_input(2);

for i = 3:(length(test_input) - 1)
    lhs_y(i) = lhs_y(i-1) - lhs_y(i-2) + test_input(i);
end

% Build up the right hand side.
rhs_y = zeros(1, length(dec_x));
for i = 1:(length(dec_x) - 1)
    rhs_y(i) = 5 .* y_x(i) + 7 .* y_y(i);
end

% Plot the difference between the two sides.
diff = rhs_y - lhs_y;

f6a = figure;
set(f6a, 'Name', 'Problem 6 Linearity Test');
subplot(3,1,1);
plot(lhs_y);
title('y(a*x_{1} + b*x_{2})')
subplot(3,1,2);
plot(rhs_y);
title('a*y(x_{1}) + b*y(x_{2})')
subplot(3,1,3);
plot(diff);
title('Difference')
print('matlab_problem_6_linearity_test','-dpdf')

% Note that the plot of the difference looks non-zero until you look at the
% scale. Since the difference is zero the whole way through, the system
% appears to be linear!

% Now we need to test whether the system is time-invariant. We will do this
% by showing that:
%     y(x(n-3)) = y(n-3)

% Build up the left hand side.
shifted_input = [];
for i = 4:(length(dec_x) - 1)
    shifted_input = [shifted_input, dec_x(i)];
end

y_shift = zeros(1, length(shifted_input));
y_shift(1) = y_x(3) - y_x(2) + shifted_input(1);
y_shift(2) = y_shift(1) - y_x(3) + shifted_input(2);

for i = 3:(length(shifted_input) - 1)
    y_shift(i) = y_shift(i-1) - y_shift(i-2) + shifted_input(i);
end

% Build up the right hand side (shift the old output by the same constant).
shifted_y = [];
for i = 4:(length(y_x) - 1)
    shifted_y = [shifted_y, y_x(i)];
end

% Compute the difference between the two.
y_diff = shifted_y - y_shift;

f6b = figure;
set(f6b, 'Name', 'Problem 6 Time Invariance Test');
subplot(3,1,1);
plot(y_shift);
title('y(x(n - k))')
subplot(3,1,2);
plot(shifted_y);
title('y(n - k)')
subplot(3,1,3);
plot(y_diff);
title('Difference')
print('matlab_problem_6_time_inv_test','-dpdf')

% Since the difference between the two is zero, the system appears to be
% time invariant!   


% Begin Problem 7

% Produce a folded version of each of our signals.
x_fold = wrev(dec_x);
y_fold = wrev(dec_y);
z_fold = wrev(dec_z);

% Now compute the cross-correlations by convolving the first signal with
% the folded version of the second signal.
r_xy = conv(dec_x, y_fold);
r_yz = conv(dec_y, z_fold);
r_xz = conv(dec_x, z_fold);

f7 = figure;
set(f7, 'Name', 'Problem 7 Cross-correlations');
subplot(3,1,1);
plot(r_xy);
title('r_{xy}(n)');
subplot(3,1,2);
plot(r_yz);
title('r_{yz}(n)');
subplot(3,1,3);
plot(r_xz);
title('r_{xz}(n)');
print('matlab_problem_7_plots','-dpdf')


% Begin Problem 8

% Part a is just explanation.

% Let x(n) be the 13-point Barker sequence
barker = zeros(1, 13);
barker(1) = 1;
barker(2) = 1;
barker(3) = 1;
barker(4) = 1;
barker(5) = 1;
barker(6) = -1;
barker(7) = -1;
barker(8) = 1;
barker(9) = 1;
barker(10) = -1;
barker(11) = 1;
barker(12) = -1;
barker(13) = 1;

% Generate some noise with mean 0 and variance 0.01.
mean = 0;
sigma = 0.1;
noise = sigma .* randn(1,200) + mean;

% Now simulate the reflected signal y(n) = a * x(t-D) + v(t)
delay = 20;
delayed_sig = zeros(1, 200);
for i = delay : delay + 12
    delayed_sig(i) = barker(i - delay + 1);    
end

alpha = 0.9;
radar_y = alpha .* delayed_sig + noise;
f8b = figure;
set(f8b, 'Name', 'Problem 8b Plots');
subplot(2,1,1);
plot(barker);
title('Transmitted Signal');
subplot(2,1,2);
plot(radar_y);
title('Received Signal');
print('matlab_problem_8b_plots','-dpdf')

% Plot the cross-correlation r_xy(l), 0 <= l <= 59 and use the plot to
% estimate the value of the delay D.
folded_radar_y = fliplr(radar_y);
radar_r_xy = conv(barker, folded_radar_y);
f8c = figure;
set(f8c, 'Name', 'Problem 8c Plots');
plot(radar_r_xy);
print('matlab_problem_8c_plots','-dpdf')

curr_max = 0;
for i = 1:length(radar_r_xy)
    if (radar_r_xy(i)) > curr_max
        curr_max = i;
    end
end

