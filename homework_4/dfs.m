function X = dfs(x, N)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This file implements the dfs function described in Homework 4 MATLAB
% Exercise 1. The function computes the Discrete Fourier Series
% coefficients of a periodic 1-D signal.
%
%   Parameters:
%       x - one full period of the input signal.
%       N - The fundamental period of the input signal.
%
%   Output:
%       X - the DTFS coefficients for the input signal.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% We can use the analysis equation for DTFS (4.2.8 in the textbook) to
% obtain the coefficients.
X = zeros(1,N);
for k = 1:N
    for n = 1:N
        X(k) = X(k) + (x(n) .* exp(-2i*pi*(k-1)*(n-1)/N));
    end
    X(k) = X(k) / N;
end
