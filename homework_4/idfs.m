function x = idfs(X, N)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This file implements the idfs function described in Homework 4 MATLAB
% Exercise 2. The function obtains the original signal given the Discrete
% Time Fourier Series Coefficients.
%
%   Parameters:
%       X - the DTFS coefficients
%       N - the fundamental period of the time-domain signal.
%
%   Output:
%       x - a full period of the original time-domain signal.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% We can use the synthesis equation (4.2.7 in the textbook) to obtain the
% original signal from the coefficients.
x = zeros(1,N);
for n = 1:N
    for k = 1:N
        x(n) = x(n) + (X(k) .* exp(2i.*pi.*(k-1).*(n-1)/N));
    end
end