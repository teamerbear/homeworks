%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This file implements the MATLAB exercises for Homework 4
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all; close all;

% -------------------------------------------------------------------------
%   Problem 1
% -------------------------------------------------------------------------

% Input x(n) = [0 1 2 3], N = 4.  Find the DTFS coefficients.
in = [0 1 2 3];
X = dfs(in, 4)

% If you check this answer against MATLAB's fft function, the result is
% different. This is because the textbook puts the (1/N) scaling factor on
% the analysis equation whereas MATLAB has that scaling factor in the
% synthesis equation. I have implemented the functions as outlined in the
% textbook. Either implementation is fine.

% -------------------------------------------------------------------------
%   Problem 2
% -------------------------------------------------------------------------

% Now obtain the original signal from the coefficients we just obtained.
x = idfs(X, 4)

% -------------------------------------------------------------------------
%   Problem 3
% -------------------------------------------------------------------------

% TODO: Why is this not true?? It matches the format exactly.
% First, we can tell just by looking at the CCDE that this is a first order
% low pass filter (see 5.4.15 in the textbook).
%
% More mathematically, find the transfer function H(z) for the system:
%   Y(z) + 0.8 z^-1 Y(z) = 0.2 X(z)
%   Y(z) [1 + 0.8 z^-1] = 0.2 X(z)
%   H(z) = 0.2 / (1 + 0.8 z^-1)
%   H(z) = 0.2 z / (z + 0.8)
%
% This has one zero at the origin and a pole at z = -0.8 (this actually
% suggests it is a high-pass filter).
%
% Move to the Fourier Domain by replacing z with e^jw
%   H(w) = 0.2 e^jw / (e^jw + 0.8)
%
w = -pi:2*pi/1000:pi;
H = 0.2 .* exp(1i .* w) ./ (exp(1i .* w) + 0.8);
mag_H = abs(H);
ang_H = angle(H);
f3 = figure;
subplot(2,1,1);
plot(w, mag_H);
title('Problem 3 \midH\mid');
subplot(2,1,2);
plot(w, ang_H);
title('Problem 3 \angleH');
print('high_pass.png','-dpng')


% -------------------------------------------------------------------------
%   Problem 4
% -------------------------------------------------------------------------

% We first transform the function into the z-domain in order to find the
% transfer function:
%   Y(z) = 0.01 X(z) + 0.05 z^-1 X(z) + 0.05 z^-2 X(z) + 0.01 z^-3 X(z)
%           + 1.75 z^-1 Y(z) - 1.18 z^-2 Y(z) + 0.28 z^-3 Y(z)
%
%   Y(z)[1 - 1.75 z^-1 + 1.18 z^-2 - 0.28 z^-3] =
%           X(z)[0.01 + 0.05 z^-1 + 0.05 z^-2 + 0.01 z^-3]
%
%   H(z) = [0.01 + 0.05 z^-1 + 0.05 z^-2 + 0.01 z^-3] /
%            [1 - 1.75 z^-1 + 1.18 z^-2 - 0.28 z^-3]
%
% Now, move to the Fourier domain.
%   H(w) = [0.01 + 0.05 e^-jw + 0.05 e^-j2w + 0.01 e^-j3w] /
%            [1 - 1.75 e^-jw + 1.18 e^-j2w - 0.28 e^-j3w]
%
H_lp = (0.01 + 0.05 .* exp(-1i .* w) + 0.05 .* exp(-2i .* w) + 0.01 .* exp(-3i .* w)) ./ ...
            (1 - 1.75 .* exp(-1i .* w) + 1.18 .* exp(-2i .* w) - 0.28 .* exp(-3i .* w));
        
mag_H_lp = abs(H_lp);
ang_H_lp = angle(H_lp);
f4 = figure;
subplot(2,1,1);
plot(w, mag_H_lp);
title('Problem 4 \midH\mid');
subplot(2,1,2);
plot(w, ang_H_lp);
title('Problem 4 \angleH');
print('low_pass.png','-dpng')

