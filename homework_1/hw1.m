% =========================================================================
%  MATLAB Exercises for Homework 1
%
%  Erik Swenson
% =========================================================================
clear all; close all;

% Open the requested file and load it into three vectors x, y, and z
% representing the respective components of the acceleration over time.
the_file = fopen('../dataset/Climb_stairs/Accelerometer-2011-03-24-10-24-39-climb_stairs-f1.txt', 'r');
x = [];
y = [];
z = [];

the_line = fgetl(the_file);
while ischar(the_line)
    % Convert the character array into integer values
    parts = str2num(the_line);
    x = [x, parts(1)];
    y = [y, parts(2)];
    z = [z, parts(3)];
    the_line = fgetl(the_file);
end

fclose(the_file);

% Convert the coded data [x, y, z] values to real values.
g = 9.8;
dec_x = (3.0 * g / 63.0) * x - (1.5 * g);
dec_y = (3.0 * g / 63.0) * y - (1.5 * g);
dec_z = (3.0 * g / 63.0) * z - (1.5 * g);

% Plot the x, y, z components of the signal before and after the
% conversion.
the_fig = figure;
set(the_fig, 'Name', 'Part 3 Plots')
subplot(2,3,1)
plot(x)
title('Encoded X')
subplot(2,3,2)
plot(y)
title('Encoded Y')
subplot(2,3,3)
plot(z)
title('Encoded Z')
subplot(2,3,4)
plot(dec_x)
title('Decoded X')
subplot(2,3,5)
plot(dec_y)
title('Decoded Y')
subplot(2,3,6)
plot(dec_z)
title('Decoded Z')
print('matlab_problem_3_plots','-dpdf')

% Generate a cosine signal c = cos(2*pi*f*t) with frequency f = 30 Hz
% defined in the time interval t = [-0.5, 0.5]s. Sample the signal with
% sampling frequencies f_s1 = 80 Hz, f_s2 = 60 Hz, f_s3 = 30 Hz.
f = 30.0;
t1 = -0.5:(1.0/80.0):0.5;
sig1 = cos(2.0 * pi * f * t1);

t2 = -0.5:(1.0/60.0):0.5;
sig2 = cos(2.0 * pi * f * t2);

t3 = -0.5:(1.0/30.0):0.5;
sig3 = cos(2.0 * pi * f * t3);

fig2 = figure;
set(fig2, 'Name', 'Part 4 Plots')
subplot(3,1,1)
plot(t1, sig1)
title('f_{s1} = 80 Hz')
subplot(3,1,2)
plot(t2, sig2)
title('f_{s2} = 60 Hz')
subplot(3,1,3)
plot(t3, sig3)
title('f_{s3} = 30 Hz')
print('matlab_problem_4_plots','-dpdf')

% Plot the sampled signals and comment as necessary.

% Generate a cosine signal c = cos(2*pi*f*t) with frequency f = 30 Hz
% defined in the time interval t = [-0.5, 0.5]s. Let us sample this using a
% sampling frequency f_s = 32 Hz.
step = 1.0 / 32.0;
t = -0.5:step:0.5;
c = cos(2.0 * pi * f * t);

% Combine this cosine signal with the x-component of the mapped signal from
% Part 3 as x_c = x + c.
t_comb = [0:(length(dec_x) - 1)]*(1/32);
c_comb = cos(2.0 * pi * f * t_comb);
x_c = dec_x + c_comb;

% Plot this signal x_c.
fig3 = figure;
set(fig3, 'Name', 'Part 5 Plots')
subplot(2,1,1)
plot(t, c)
title('Original Signal, F_s = 32 Hz')
subplot(2,1,2)
plot(t_comb, x_c)
title('Combined Signal, F_s = 32 Hz')
print('matlab_problem_5_plots','-dpdf')



