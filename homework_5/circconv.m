function y = circconv(x1, x2, N)
%CIRCCONV Computes the circular convolution of two input sequences.
%   x1 is the first sequence
%   x2 is the second sequence
%   N is the number of points in the convolution s.t.
%       length(x1), length(x2) <= N

% First, zero pad the sequences if needed.
while length(x1) < N
    x1 = cat(2, x1, 0);
end

while length(x2) < N
    x2 = cat(2, x2, 0);
end

% Allocate the output sequence
y = zeros(1, N);

% Implement the circular convolution.
for i = 1:N
    y(i) = sum(x1 .* erik_circshift(erik_circflip(x2), (i-1), N));
end

end

