clear all; close all;

% -------------------------------------------------------------------------
%   Problem 1
% -------------------------------------------------------------------------

% Original sequence.
n = 0:10;
x = 10 .* (0.6 .^ n);

% Compute the circular shift x((n-6))_16
N = 16; 
shift = 6;
x_shifted = erik_circshift(x, shift, N);

figure;
subplot(2,1,1);
plot(n, x);
title('Original Signal');
subplot(2,1,2);
plot(0:15, x_shifted);
title('Circular Shifted Signal');
print('circ_shift.png','-dpng')

% -------------------------------------------------------------------------
%   Problem 2
% -------------------------------------------------------------------------

% First, compute the circular convolution of x1 and x2 for N = 4.
x1 = [1 4 3];
x2 = [1 3 6 9];
circ_x = circconv(x1, x2, 4)

% Then compute the linear convolution of the two sequences.
lin_x = conv(x1, x2)

% Now, choose N such that the two are equal.
eq_x = circconv(x1, x2, 6)

% -------------------------------------------------------------------------
%   Problem 3
% -------------------------------------------------------------------------

% Test the overlap-save with circular convolution.
n = 0:10;
x_n = n.^2 + 1;
h = [1 0 -1];
N = 6;
y_n = overlap_save(x_n, h, N);

% Verify that it's the same as the linear convolution of the time domain
% signal with the impulse response.
y_verify = conv(x_n, h);

figure;
subplot(2,1,1);
plot(n, y_n);
title('Overlap-Save (Circular Convolution)');
subplot(2,1,2);
plot(n, y_verify(1:11));
title('Linear Convolution (Verification)');
print('overlap_save_circconv.png','-dpng')

% -------------------------------------------------------------------------
%   Problem 4
% -------------------------------------------------------------------------

% The sequence for which we want to take the DFT.
x_n = [1 2 4 8 16 32 32 16 8 4 2 1];

% The DFT of the sequence.
x_w = fftshift(fft(x_n));
mag_x_w = abs(x_w);
phase_x_w = angle(x_w);

N = length(x_n);
w_sampled = -pi:(2.*pi/N):(pi-(2.*pi/N));
figure;
subplot(3,1,1);
plot(0:11, x_n);
title('x(n)');
subplot(3,1,2);
plot(w_sampled, mag_x_w);
title('\midX(\omega)\mid');
subplot(3,1,3);
plot(w_sampled, phase_x_w);
title('\angleX(\omega)');
print('sampled_dft.png', '-dpng');

% Find the analytical DTFT of the signal and plot it's magnitude and phase.
w = -pi:(2.*pi/200):pi;
dtft_w = zeros(1, length(w));
for i = 1:length(w)
    for j = 1:length(x_n)
        dtft_w(i) = dtft_w(i) + (x_n(j) .* exp(-1i.*w(i).*(j-1)));
    end
end

figure;
half_w = floor(length(w)/2);
subplot(2,1,1);
plot(w, abs(dtft_w));
title('DTFT \midX(e^{j\omega})\mid')
subplot(2,1,2);
plot(w, angle(dtft_w));
title('DTFT \angleX(e^{j\omega})');
print('analytical_dtft.png', '-dpng');


% Plot the sampled DFT over the DTFT and verify that they agree.
figure;
plot(w, abs(dtft_w));
hold on;
plot(w_sampled, mag_x_w, 'x');
title('DTFT vs. Sampled DFT')
print('dtft_vs_dft.png', '-dpng');

% -------------------------------------------------------------------------
%   Problem 5
% -------------------------------------------------------------------------

% Test the overlap-save with circular convolution.
n = 0:10;
x_n = n.^2 + 1;
h = [1 0 -1];
N = 6;
y_n_fft = overlap_save_fft(x_n, h, N);

figure;
subplot(2,1,1);
plot(n, y_n_fft);
title('Overlap-Save (DFT/IDFT)');
subplot(2,1,2);
plot(n, y_verify(1:11));
title('Linear Convolution (Verification)');
print('overlap_save_dft.png', '-dpng');



