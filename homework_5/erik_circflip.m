function out = erik_circflip(seq)
%ERIK_CIRCFLIP Flips a sequence as if it were laid out in a circle (like
%for a circular convolution.
%   seq is the sequence to flip.

% To acheive a circular flip, we first do a circular shift of -1 and then
% reverse the elements. This keeps the first element at the front, but
% "spins" the other direction.
out = erik_circshift(seq, -1, length(seq));
out = flip(out);
end

