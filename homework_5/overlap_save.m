function y = overlap_save(x, h, N)
%OVERLAP_SAVE Implements the overlap-save method of the DFT.
%   x is the (long) input sequence
%   h is the impulse response of the FIR filter (length M).
%   L is the size of the blocks we'll process
%   y is the output sequence

y = zeros(1, length(x));
M = length(h);
L = N - M + 1;
last_chop = zeros(1, M-1);

% Append L-1 zeros onto the impulse response.
h_new = zeros(1, N);
for i = 1:length(h)
    h_new(i) = h(i);
end

% Implement overlap-save via circular convolution for each chunk.
for i = 1:ceil(length(x)/L)
    buff = zeros(1,N);
    % Copy the appropriate data into a buffer of length N
    curr_block = (L * (i - 1)) + 1;
    for j = 1:M-1
        buff(j) = last_chop(j);
    end
    for j = M:N
        if (curr_block + (j-M)) > length(x)
            buff(j) = 0;
        else
            buff(j) = x(curr_block + (j-M));
        end
        
        % Fill in last_chop with the last M-1 data points of this chunk
        % in preparation for the next iteration.
        if j > L
            last_chop(j - L) = buff(j);
        end
    end
    
    % Perform the circular convolution with the buffer and the extended
    % impulse response and put the data in the output sequence.
    result = circconv(buff, h_new, N);

    if (curr_block + (L - 1)) > length(x)
        diff = length(x) - curr_block;
        y(curr_block:length(x)) = result(M:(M + diff));
    else
        y(curr_block:(curr_block + (L - 1))) = result(M:N);
    end
end

end

