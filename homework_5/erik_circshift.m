function out = erik_circshift(seq, shift, N)
%CIRCSHIFT Perform a circular shift on the input sequence.
%   seq is the sequence to circularly shift
%   shift is the number of data points to shift by. The sign gives the
%           direction of the shift (positive number means positive shift).
%   N is the number of data points to consider in the circle.

% Zero pad the signal, if needed.
while length(seq) < N
    seq = cat(2, seq, 0);
end

% We pretend that the array is a circular buffer and we just move the
% starting index by the shift.
out = zeros(1, length(seq));
for i = 1:length(seq)
    % If the index goes past the end of the array, wrap around to the
    % beginning.
    shift_ind = mod(i + shift, N);
    if (shift_ind == 0)
        shift_ind = N;
    end
    
    out(shift_ind) = seq(i);
end

end

