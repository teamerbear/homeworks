function out = erik_dft(seq, N)
%ERIK_DFT Compute the DFT the "old fashioned" way
%   seq is the time domain input sequence to transform
%   N is the number of points to use in the DFT
%   out is the frequency domain output sequence

out = zeros(1,N);
for k = 1:N
   for n = 1:N
       out(k) = out(k) + (seq(n) .* exp(-1i*2*pi*(k-1)*(n-1)./N));
   end
end
end

