% =========================================================================
%  MATLAB Exercises for Homework 3
%
%  Erik Swenson
% =========================================================================

clear all; close all;

% =========================================================================
% Problem 1
% =========================================================================

% Create our signal x(n)
n = 0:1:999;
alpha = 0.6;
x_n = alpha .^ n;

% Evaluate the z-transform when z = 1
z = 1;
numeric_X = sum(x_n .* z .^ -n)

% =========================================================================
% Problem 2
% =========================================================================

% Initialize some variables and create the transforming function (in this
% case a complex exponential).
w = 0:0.01:pi;
max_n = 4;
z = exp(1i*w);

% Compute the z-transform for n in [1,4]
X_trans_4 = zeros([1,length(z)]);
for i = 1:length(z)
   for j = 1:max_n
      X_trans_4(i) = X_trans_4(i) + (1/j) * z(i)^-j; 
   end
end

max_n = 8;
X_trans_8 = zeros([1, length(z)]);
for i = 1:length(z)
    for j = 1:max_n
        X_trans_8(i) = X_trans_8(i) + (1/j) * z(i)^-j;
    end
end

f2 = figure;
hold on;
plot(w, abs(X_trans_4), 'Color', 'b', 'LineWidth', 2)
plot(w, abs(X_trans_8), 'Color', 'r', 'LineWidth', 2)
title('Problem 2: Z-Transform')
legend('k = 4', 'k = 8');
hold off;
print('hw3_prob2_plots', '-dpdf')


% =========================================================================
% Problem 3
% =========================================================================

% Initialize a grid in the complex plane.
z_real = -2:0.01:2;
z_imag = (-2:0.01:2) * 1i;

X_trans = zeros([length(z_real), length(z_imag)]);

% Compute the z-transform at each point in the complex plane.
for i = 1:length(z_real)
   for j = 1:length(z_imag)
      X_trans(i,j) =  sum(x_n .* (z_real(i) + z_imag(j)).^(-n));
   end
end

% Plot the magnitude of the z-transform.
f3 = figure;
X_trans(isnan(X_trans(:))) = Inf;
imagesc(z_real, z_real, abs(X_trans))
axis xy; axis  square; grid
title('Problem 3: |X(z)|, Z-transform evaluation')
colormap('gray')
colorbar
caxis([-20 20 ])
viscircles([0,0], 1, 'Color', 'b');
viscircles([0,0], alpha, 'Color', 'r');
print('hw3_prob3_plots', '-dpdf')

% We expect the ROC to be outside the circle since this is a causal signal.


% =========================================================================
% Problem 4
% =========================================================================

% Now generate the prescribed anti-causal signal.
opp_n = -999:1:-1;
opp_sig = -alpha .^ -opp_n;
X_opp_trans = zeros([length(z_real), length(z_imag)]);

% Compute the z-transform for each point in the z-plane.
for i = 1:length(z_real)
   for j = 1:length(z_imag)
      X_opp_trans(i,j) =  sum(opp_sig .* (z_real(i) + z_imag(j)).^(-opp_n));
   end
end

% Plot the magnitude of the z-transform.
f4 = figure;
X_opp_trans(isnan(X_opp_trans(:))) = Inf;
imagesc(z_real, z_real, abs(X_opp_trans))
axis xy; axis  square; grid
title('Problem 4: |X(z)|, Z-transform evaluation')
colormap('gray')
colorbar
caxis([-20 20 ])
viscircles([0,0], 1, 'Color', 'b');
viscircles([0,0], 1/alpha, 'Color', 'r');
print('hw3_prob4_plots', '-dpdf');

% We expect the ROC to be inside the circle since this is an anti-causal
% signal.


